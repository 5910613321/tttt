json.extract! my_user, :id, :email, :created_at, :updated_at
json.url my_user_url(my_user, format: :json)
