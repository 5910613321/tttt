Feature: Manage users
  In order to [goal]
  [stakeholder]
  wants [behaviour]
  
  Scenario: Register new users
    Given I am on the new users page
    When I fill in "Email" with "email 1"
    And I fill in "Password" with "password 1"
    And I fill in "Password confirmation" with "password 1"
    And I press "Create User"
    Then I should see "email 1"

