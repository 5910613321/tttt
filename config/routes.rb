Rails.application.routes.draw do

  

  resources :my_users
  get 'logout', to: 'sessions#destroy', as: 'logout'
  resources :users
  resources :sessions, only: [:new, :create, :destroy]
  get '/signup', to: 'users#new'
  get '/login', to: 'sessions#new'
  get '/home',  to: 'users#home'
  get '/about', to: 'users#about'
  root 'users#home'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
